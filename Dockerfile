FROM gitlab-registry.cern.ch/ai-config-team/ai-tools

LABEL maintainer="Daniel Juarez Gonzalez djuarezg@cern.ch"

COPY cleanup.py /
